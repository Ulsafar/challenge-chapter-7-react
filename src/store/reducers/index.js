import { combineReducers } from "redux";
import filterReducer from "./FilterReducers";

const reducer = combineReducers({
    filter: filterReducer
})

export default reducer